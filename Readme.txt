This Readme will and should contain any information vital to this repo.

The team members of this repo are:
-	Joel Hodge (joel.hodge@student.qut.edu.au)
-	Matt Spencer (matthew.spencer@student.qut.edu.au)
-	Nicholas Kanes (nicholas.kanes@student.qut.edu.au)
-	Sam Mostert (samuel.mostert@student.qut.edu.au)
-	Sukreet Flora (sukreet.flora@student.qut.edu.au)


Purpose of repo:
To allow the easy and efficient version control of the ENB241 assignment

Comments:
An overview and very explanation of how to use source tree can be found here: 
http://blackish.at/blog/wp-content/uploads/2013/04/UnityBitBucketSourceTree_ForAll.pdf