#ifndef DF_TRACKER_H
#define DF_TRACKER_H

#include "DF_Model.h"
#include "Dist_Field.h"

#include <vil/vil_image_view.h>

class DF_Tracker
{
public:

	/* Constructor ************************************************************************
	This is the default constructor for this class, it simply takes input arguments and then
	initialises all member variables to the provided values.
	Inputs:
	*:- channels = variable used to initalise _channels member variable(MV)
	*:- spatial_blur = variable used to initialise _spatial_blur MV
	*:- colour_blur = variable used to initialise _colour_blur MV
	*:- search_dist = variable used to initialise _search_dist MV
	*:- learning_rate = variable used to initialise _learning_rate MV
	*:- colour = boolean true or false as to if input image is colour or grayscale */
	DF_Tracker(unsigned int channels, unsigned int spatial_blur,
			   unsigned int colour_blur, unsigned int search_dist, float learning_rate);

	/* Destructor *************************************************************************
	This is the default destructor for this class, it deletes any dynamically created memory
	NOTE: All dynamic memory must be deleted to prevent memory leaks / seg faults.
	dynamic memory to delete:
	*1) model: single object to delete.
	*2) */
    ~DF_Tracker();

	/* Init Function **********************************************************************
	Init takes an image, and its start location, along with the width and height of the
	image. It then uses the vil_crop function to create a cropped version of the image, the
	cropped image will be the size of width and height.
	Once the cropped image has been created, it is saved as a new vil_image_view.
	the model member variable is dynamically allocated memory and then initialised using the
	cropped image along with its x and y location.
	Inputs:
	*:- image = the image of whichever frame is first in the sequence
	*:- start_x = the x location of the object we are aiming to track
	*:- start_y = the y location of the object we are aiming to track
	*:- width = the width of the object we are aiming to track
	*:- height = the height of the object we are aiming to track

	Outputs: Nothing, function is void.
	NOTE: This function is only used to initialise the model for the first frame of the sequence*/
	void Init(vil_image_view<unsigned char> image, unsigned int start_x, unsigned int start_y, unsigned int width, unsigned int height);

    /* ColourInit function *****************************************************************
	Does the same job as the Init function, except it accepts colour images and creates a
	DF for each of the colour planes.
	Inputs:
	*:- image = the image of whichever frame is first in the sequence
    *:- start_x = the x location of the object we are aiming to track
	*:- start_y = the y location of the object we are aiming to track
	*:- width = the width of the object we are aiming to track
	*:- height = the height of the object we are aiming to track*/
    void ColourInit(vil_image_view<unsigned char> image, unsigned int start_x, unsigned int start_y, unsigned int width, unsigned int height);

	/* Track Function *********************************************************************
	Track takes a new image which is simply a frame from the video sequence. The function then
	searches for the new location of the image which has been initalised as the image which is
	being tracked.

	Inputs:
	*:- image: the new frame which needs to have the image tracked in
	*:- count: address of external count to see what image is being processed
	*:- output_path: path of desired output folder for images to be saved to
	*:- yn: Do you want this function to run y=yes, n=no

	Outputs:
	*:- None, function is void.*/
	void Track(vil_image_view<unsigned char> image, int& counter, vcl_string output_path, vcl_string yn);


	/* Draw Box function *******************************************************************
	 This function is used to draw a box on the image to visualize the current location of the object
	 being tracked. It will do this by modifying the current vil_image_view object then finally
	 saving it in the specified output folder.

	 Inputs:
	 	*:- image: Current image
	 	*:- output_path: Output location
	 	*:- count: address of external count to see what image is being processed
	 	*:- yn: Do you want this function to run y=yes, n=no

	 Outputs:
	 	*:- None, function is void.*/
	void DrawBox(vil_image_view<unsigned char> image, vcl_string output_path, int& count, vcl_string yn) const;


protected:

	// Members

	const unsigned int _channels;		// Number of channels for the DF
	const unsigned int _spatial_blur;	// Width of spatial blurring to be applied across a channel
	const unsigned int _colour_blur;	// Width of blurring through the channels to be applied
	const unsigned int _search_dist;	// Distance to search for movement
	const float _learning_rate;		//Rate at which the algorithm will learn

	unsigned int _prev_x;
	unsigned int _prev_y;
	unsigned int _width;		// Width of the DF
	unsigned int _height;		// Height of the DF


	// DF_Model class will store all the information about the object we are tracking such as
	// size, x & y location in the image.
	DF_Model* model;


};

#endif
