#include "Dist_Field.h"

#include <vil/vil_image_view.h>
#include <vil/vil_crop.h>
#include <vil/algo/vil_gauss_filter.h>
#include <vil/vil_save.h>

#include <vbl/vbl_array_2d.h>

#include <vcl_vector.h>
#include <vcl_fstream.h>
#include <vcl_iostream.h>

#include <vnl/vnl_random.h>

# include <vcl_cmath.h>


Dist_Field::Dist_Field() : _sigma_2d(1), _sigma_1d(0.625)
{
    debug_txt = new vcl_ofstream;
    _debug = false;

}

Dist_Field::Dist_Field(bool debug) : _sigma_2d(1), _sigma_1d(0.625)
{
    debug_txt = new vcl_ofstream;
    _debug = debug;

}

Dist_Field::Dist_Field(double sd_2d, double sd_1d) : _sigma_2d(sd_2d), _sigma_1d(sd_1d)
{
     // Initialises const members
}

Dist_Field::Dist_Field(bool debug, double sd_2d, double sd_1d) : _sigma_2d(sd_2d), _sigma_1d(sd_1d)
{
    debug_txt = new vcl_ofstream;
    _debug = debug;

}

Dist_Field::~Dist_Field()
{
   // If debug is active, delete the memory allocated to ofstream
    if(_debug){
        *debug_txt << "Closing and Deleting debug_txt memory" << vcl_endl;
        debug_txt->close();
        delete debug_txt;
    }

}

void Dist_Field::Init(vil_image_view<unsigned char>& image, unsigned int channels, unsigned int spatial_blur, unsigned int colour_blur)
{

    // Initialise members with values
    _width = image.ni();
    _height =image.nj();
    _channels = channels;
    _channel_width = 256/_channels;
    _spatial_blur = spatial_blur;
    _colour_blur = colour_blur;

    if(_debug) {
        debug_txt->open("Debug Output.txt", vcl_ios::app);
        // Print to screen the intialisation parameters of the DF
        *debug_txt << "Building Distribution Field with Parameters:" << vcl_endl;
        *debug_txt << "Size: " << _width << "x" << _height << vcl_endl;
        *debug_txt << "Channels: " << _channels <<  vcl_endl;
        *debug_txt << "Channel Width: " << _channel_width <<  vcl_endl;
        *debug_txt << "Spatial Blur Width:  " << _spatial_blur << " Sigma:  " <<  _sigma_2d <<  vcl_endl;
        *debug_txt << "Colour Blur Width:  " << _colour_blur << " Sigma:  " <<  _sigma_1d <<  vcl_endl << vcl_endl;
    }

// Fill DF_Array with vil_image_views of a single plane
    for(unsigned int i = 0; i < _channels; i++) {
        vil_image_view<double> temp(image.ni(), image.nj(), 1); // Create temp vil_image_view of size of image, single plane
        temp.fill(0);
        _DF.push_back(temp); //  Add temp to vector
    }

    // Assign the pixel channels
    for(unsigned int x = 0; x < _width; x++) {
        for(unsigned int y = 0; y < _height; y++) {
            unsigned int channel = image(x,y)/_channel_width;
            _DF[channel](x,y) = 1.0;
        }
    }

    if(_debug) {
        *debug_txt << "Values after setting each pixel in each channel." << vcl_endl << vcl_endl;
        PrintDFValues();
    }



// Call Blur2D which will perform a Gaussian 2D Blur on each individual channel
    Blur_2D();

    if(_debug) {
        *debug_txt << "Values after applying 2D Spatial Blur with width: " << spatial_blur << vcl_endl;
        PrintDFValues();
    }

// Perform a 1D gaussian filter at each individual pixel through the channels.
    Blur_1D();

    if(_debug) {
        *debug_txt << "Values after applying 1D Colour Blur with width: " << colour_blur << vcl_endl;
        PrintDFValues();
    }
    if(_debug){
        SaveDF();
    }
}

void Dist_Field::InitCrop(Dist_Field input, unsigned int width, unsigned int height)
{
    _width = width;
    _height = height;
    _channels = input._channels;
    _channel_width = 256/_channels;
    _spatial_blur = input._spatial_blur;
    _colour_blur = input._colour_blur;

    if(_debug) {
        debug_txt->open("Debug Output.txt", vcl_ios::app);
        // Print to screen the intialisation parameters of the DF
        *debug_txt << "Building Cropped Distribution Field From Another Dist_Field Object" << vcl_endl;
        *debug_txt << "Size: " << _width << "x" << _height <<  vcl_endl;
    }

    // Fill DF with 0's
    for(unsigned int i = 0; i < _channels; i++) {
        vil_image_view<double> temp(width, height, 1);
        temp.fill(0);
        _DF.push_back(temp); //  Add temp to vector
    }

}


void Dist_Field::Blur_2D()
{

    for(unsigned int i = 0; i < _channels; i++) {
        vil_image_view<double> temp_output(_width,_height,1);  // Create a temporary output
        vil_gauss_filter_2d(_DF[i], temp_output, _sigma_2d, _spatial_blur, vil_convolve_zero_extend);  // Apply the gaussian filter and store in temp_output
        _DF[i] = temp_output; // Store the new blurred image back in it's respective DF_array spot
    }

}

void Dist_Field::Blur_1D()
{
    for(unsigned int x = 0; x < _width; x++) {
        for(unsigned int y = 0; y < _height; y++) {
            vil_image_view<double> temp1D(_channels,1,1); // Make a temp 1D vil_image view of size num_channels by 1
            temp1D.fill(0);  // Initialise all values to 0
            for(unsigned int i = 0; i < _channels; i++) {
                temp1D(i,0) = _DF[i](x,y); // Push the individual pixel from channel i into the respective slot in the array
            }
            vil_image_view<double> temp1D_output(_channels,1,1);
            vil_gauss_filter_1d(temp1D, temp1D_output, _sigma_1d, _colour_blur); // Apply the gauss filter
            for(unsigned int i = 0; i < _channels; i++) {
                _DF[i](x,y) = temp1D_output(i,0); // Work backwards pushing the now filtered results back into the DF
            }
        }
    }
}

void Dist_Field::DF_Update(Dist_Field& input, float learning_rate)
{
    if(_debug) {*debug_txt << "Updating DF with a learning rate of: " << learning_rate << vcl_endl;}
    for(int c = 0; c < _channels; c++) {
        for(int x = 0; x < _width; x++) {
            for(int y = 0; y < _height; y ++) {
                _DF[c](x,y) = (*this)(x,y,c)*(1-learning_rate) + input(x,y,c)*learning_rate;
            }
        }
    }

    if(_debug) {PrintDFValues();}

}

void Dist_Field::DF_Crop(Dist_Field& input, unsigned int x, unsigned int width, unsigned int y, unsigned int height)
{

    this->InitCrop(input, width, height);
    for(unsigned int i = 0; i < input._channels; i++) {
        vil_image_view<double> temp = input._DF[i];
        this->_DF[i] = vil_crop(temp, x, width, y, height);
    }

    if(_debug) {
        this->PrintDFValues();
    }

}

void Dist_Field::PrintDFValues() const
{

    *debug_txt << "Printing Values of DF Array" << vcl_endl;
    for(unsigned int i = 0; i < _channels; i++) {
        *debug_txt << "Channel " << i << ":" <<  vcl_endl;
        for(unsigned int y = 0; y < _height; y++) {
            for(unsigned int x = 0; x < _width; x++) {
                debug_txt->precision(3);
                *debug_txt << "|" << (*this)(x,y,i);
            }
            *debug_txt << "|" << vcl_endl;
        }
    }
    *debug_txt << vcl_endl;
}

void Dist_Field::SaveDF() const
{
    vil_image_view<unsigned char> output(_width, _height, 1); // Creating an output image
    output.fill(0);
    for(unsigned int i = 0; i < _channels; i++) {
        for(unsigned int x = 0; x < _width; x++) {
            for(unsigned int y = 0; y < _height; y ++) {
                output(x,y) = _DF[i](x,y) * 255; // Convert the DF values to 255 equivalents
            }
        }
        vcl_stringstream outputFileName;
        outputFileName << "DF_" << i << ".png";
        vil_save(output,outputFileName.str().c_str());
        output.fill(0);
    }


}

// Field(x,y,c) returns the value at (x,y) coordinate of channel c
double Dist_Field::operator() (unsigned int x, unsigned int y , int c) const
{
    return (_DF[c](x,y));
}

// Return the total sum of the two DFs (for DF1 + DF2, size of DF2 >= DF1)
double Dist_Field::operator- (Dist_Field& df2) const
{
    double distance = 0;

    for (unsigned int c = 0; c < _channels; c++) {
        for (unsigned int x = 0; x < _width; x++) {
            for (unsigned int y = 0; y < _height; y++) {
                distance  += fabs((*this)(x,y,c) - (df2(x,y,c)));
            }
        }
    }

    return distance;
}
