// Please put all includes needed for main to run below:
#include "DF_Model.h"
#include "Dist_Field.h"

#include <vil/vil_image_view.h>
#include <vil/vil_load.h>
#include <vil/vil_save.h>

#include <vul/vul_file_iterator.h>
#include <vul/vul_file.h>

#include <vcl_vector.h>

#include <vul/vul_arg.h>

#include <sstream>
#include <cstdio>

#include <vcl_fstream.h>

using namespace::std;


/*
* Below is all the function prototypes used in this test driver
*/

// Create an ofstream object to output to txt file
ofstream debug_txt;

// Function prototypes for Dist_Field
void testChannels();
void testBlurring();
void testCrop();
void testSubtract();

// Start of main function and implementation of the test driver

int main (int argc, char * argv[])
{

    debug_txt.open("Debug Output.txt", vcl_ios::app);
    debug_txt << "//****************************************************************//" << vcl_endl;
    debug_txt << "//***************START Test Drivers For Dist_Fields***************//" << vcl_endl;
    debug_txt << "//****************************************************************//" << vcl_endl << vcl_endl;

    // Test the Constructors and Channel Allocation
    testChannels();

    debug_txt << vcl_endl << vcl_endl << vcl_endl << vcl_endl;

    // Test the 2D and 1D Blurring Functions
    testBlurring();

    debug_txt << vcl_endl << vcl_endl << vcl_endl << vcl_endl;

    // Test the DF_Crop() Function
    testCrop();

    debug_txt << vcl_endl << vcl_endl << vcl_endl << vcl_endl;

    // Test the Overloaded Subtract Operator
    testSubtract();

    debug_txt << "//****************************************************************//" << vcl_endl;
    debug_txt << "//*************** END Test Drivers For Dist_Fields ***************//" << vcl_endl;
    debug_txt << "//****************************************************************//" << vcl_endl<< vcl_endl;

/* END OF MAIN */

    debug_txt.close();
}

//****************************************************************//
//***************START Test Drivers For Dist_Fields***************//
//****************************************************************//
// This test will test the channel allocation algorithm in the Dist_Field class
// For a known number of channels we can predict what channel a specific pixel value will be assigned to
//
// This is tested with a simple case of a 4 channel Dist_Field, where it is supplied a 2x2 dummy image
// Where each of the 4 pixels is given a specific value.
//
// We can calculate what channel the pixel should be allocate, and then verify this by reading the output
// stored in Debug.txt
void testChannels(){

    // Build a single plane dummy image
    vil_image_view<unsigned char> image(2,2,1);
    debug_txt << "Simple Case to Test Channel Allocation" << vcl_endl;
    debug_txt << "Setting up 2x2x1 vil_image_view with following pixel values: " << vcl_endl;
    debug_txt << "|0|100|" << vcl_endl << "|150|200|" << vcl_endl << vcl_endl;

    // Assign each pixel a unique value which we know what channel it should end up in
    image(0,0) = 0;   // Channel 0
    image(1,0) = 100; // Channel 1
    image(0,1) = 150; // Channel 2
    image(1,1) = 200; // Channel 3

    debug_txt << "Testing Constructor and Channel Allocation" << vcl_endl;
    debug_txt << "Actual Input Parameters: "<< vcl_endl;
    debug_txt << "Size:  2x2 " << vcl_endl;
    debug_txt << "Channels: 4" << vcl_endl << "Channel Width: 64" << vcl_endl;
    debug_txt << "Spatial Blur Width:  0       Spatial Blur Sigma:  0" << vcl_endl;
    debug_txt << "Colour Blur Width :  0       Colour Blur Sigma :  0" << vcl_endl << vcl_endl;

    // Constructing Dist_Field object with debug on, and arbitrary spatial and colour sigma values to test constructor
    Dist_Field test(true, 1, 1);

    debug_txt << "Channel Allocation should be as follows: " << vcl_endl;
    debug_txt << "0   - 63  - > Channel 0" << vcl_endl;
    debug_txt << "64  - 127 - > Channel 1" << vcl_endl;
    debug_txt << "128 - 191 - > Channel 2" << vcl_endl;
    debug_txt << "192 - 255 - > Channel 3" << vcl_endl << vcl_endl;

    // Running Dist_Field Init to produce debugging output
    test.Init(image, 4, 0, 0 );

}


// This function tests both the 2D and 1D Gaussian blurring components of the
// distribution field algorithm.
//
// The actual output values aren't checked, as we don't know enough about the gaussian algorithm
// being used to make such predictions.
//
// This test driver therefore aims to show two outcomes:
// 1. That the 2D blur only distributes the values in a specific channel throughout its
//    own channels.  Ie, we are looking for only blurring in the x,y coordinates.
// 2. That the 1D blur distributes only distributes a value through the channels, at a specific
//    x,y location.
void testBlurring(){

    // To test the 2D Blurring we make another 2x2 dummy image with the following setup
    debug_txt << "Simple Case to Test 2D Gaussian Blurring" << vcl_endl;
    debug_txt << "Setting up 4x4x1 vil_image_view with following pixel values: " << vcl_endl;
    debug_txt << "|0|0|0|100|" << vcl_endl << "|0|0|0|0|" << vcl_endl;
    debug_txt << "|0|0|0|0|" << vcl_endl << "|150|0|0|200|" << vcl_endl << vcl_endl;

    // Create dummy image
    vil_image_view<unsigned char> image(4,4,1);

    // Fill with 0's but assign values to the corners
    image.fill(0);    // Channel 0
    image(3,0) = 100; // Channel 1
    image(0,3) = 150; // Channel 2
    image(3,3) = 200; // Channel 3

    // We then setup a Dist_Field with simple blurring parameters as shown below
    // Verification is performed by looking at channels 1-3 where it can easily be seen
    // that the blurring is only taking affect across those x,y coordinates.
    debug_txt << "Actual Input Parameters: "<< vcl_endl;
    debug_txt << "Size:  4x4 " << vcl_endl;
    debug_txt << "Channels: 4" << vcl_endl << "Channel Width: 64" << vcl_endl;
    debug_txt << "Spatial Blur Width:  1       Spatial Blur Sigma:  1" << vcl_endl;
    debug_txt << "Colour Blur Width :  0       Colour Blur Sigma :  0.625" << vcl_endl << vcl_endl;

    // Running Dist_Field Init to produce debugging output
    Dist_Field test2D(true);
    test2D.Init(image,4,1,0);

    debug_txt << vcl_endl << vcl_endl << vcl_endl << vcl_endl;

    // To test the 1D blurring, we make a similar 2x2 dummy image
    debug_txt << "Simple Case to Test 1D Gaussian Blurring" << vcl_endl;
    debug_txt << "Setting up 4x4x1 vil_image_view with following pixel values: " << vcl_endl;
    debug_txt << "|0|0|0|0|" << vcl_endl << "|0|0|0|0|" << vcl_endl;
    debug_txt << "|0|0|0|0|" << vcl_endl << "|100|0|0|200|" << vcl_endl << vcl_endl;

    // Fill with 0's but assign values to bottom corners
    image.fill(0);    // Channel 0
    image(0,3) = 100; // Channel 2
    image(3,3) = 200; // Channel 3

    // By increasing the number of channels, it will be easier to verify a
    debug_txt << "Actual Input Parameters: "<< vcl_endl;
    debug_txt << "Size:  4x4 " << vcl_endl;
    debug_txt << "Channels: 8" << vcl_endl << "Channel Width: 32" << vcl_endl;
    debug_txt << "Spatial Blur Width:  0       Spatial Blur Sigma:  1" << vcl_endl;
    debug_txt << "Colour Blur Width :  1       Colour Blur Sigma :  0.625" << vcl_endl << vcl_endl;

    Dist_Field test1D(true);
    test1D.Init(image,8,0,1);
}

// Test Function for the Crop function of the dist field class
// Starts by creating a dummy image and then a Dist_Field object from that image
//
// The original Dist_Field object is then used as the base to create
// 3 cropped versions of the original Dist_Field
//
// The cropped versions differ in shape and starting location to ensure that the
// code used to crop is correct.
void testCrop(){

    debug_txt << "Simple Case to Test DF_Crop()" << vcl_endl;
    debug_txt << "Setting up 4x4x1 vil_image_view with following pixel values: " << vcl_endl;
    debug_txt << "|0|0|255|255|" << vcl_endl << "|0|0|255|255|" << vcl_endl;
    debug_txt << "|0|0|255|255|" << vcl_endl << "|0|0|255|255|" << vcl_endl << vcl_endl;

    // Create dummy image
    vil_image_view<unsigned char> image(4,4,1);

    image.fill(0);
    // Set the 2 right sides to 255 to set to channel 1
    for(int x = 2; x < 4; x++){
        for(int y = 0; y < 4; y++){
            image(x,y) = 255;
        }
    }

    // Create the original Dist_Field that will be cropped
    debug_txt << "Actual Input Parameters for Original DF: "<< vcl_endl;
    debug_txt << "Size:  4x4 " << vcl_endl;
    debug_txt << "Channels: 2" << vcl_endl << "Channel Width: 128" << vcl_endl;
    debug_txt << "Spatial Blur Width:  0       Spatial Blur Sigma:  1" << vcl_endl;
    debug_txt << "Colour Blur Width :  0       Colour Blur Sigma :  0.625" << vcl_endl << vcl_endl;

    Dist_Field test;  // Create test DF
    test.Init(image,2,0,0); // Will apply no blurring to make it easier to compare.

    debug_txt << "Actual Input Parameters for Cropped DF: "<< vcl_endl;
    debug_txt << "Input: Previously Created 4x4 DF" << vcl_endl;
    debug_txt << "Starting Location For Crop (Top Left Corner) In x,y Coordinates: 1,1" <<vcl_endl;
    debug_txt << "Size of Crop:  2x2" << vcl_endl << vcl_endl;

    debug_txt << "Expecting Output DF to Look Like: " << vcl_endl;
    debug_txt << "Channel 0: " << vcl_endl;
    debug_txt << "|1|0|" << vcl_endl << "|1|0|" << vcl_endl;
    debug_txt << "Channel 1: " << vcl_endl;
    debug_txt << "|0|1|" << vcl_endl << "|0|1|" << vcl_endl << vcl_endl;

    // Creating new Dist_Field object to be the cropped version
    Dist_Field* cropped;
    cropped = new Dist_Field(true);
    cropped->DF_Crop(test, 1, 2, 1, 2);  // Passing in arguments to create a 2x2 cropped Dist_Field

    delete cropped;

    debug_txt << "Actual Input Parameters for Cropped DF: "<< vcl_endl;
    debug_txt << "Input: Previously Created 4x4 DF" << vcl_endl;
    debug_txt << "Starting Location For Crop (Top Left Corner) In x,y Coordinates: 0,0" <<vcl_endl;
    debug_txt << "Size of Crop:  4x2" << vcl_endl << vcl_endl;

    debug_txt << "Expecting Output DF to Look Like: " << vcl_endl;
    debug_txt << "Channel 0: " << vcl_endl;
    debug_txt << "|1|1|0|0|" << vcl_endl << "|1|1|0|0|" << vcl_endl;
    debug_txt << "Channel 1: " << vcl_endl;
    debug_txt << "|0|0|1|1|" << vcl_endl << "|0|0|1|1|" << vcl_endl << vcl_endl;

    // Creating new Dist_Field object to be the cropped version
    cropped = new Dist_Field(true);
    cropped->DF_Crop(test, 0, 4, 0, 2);  // Passing in arguments to create a 4x2 cropped Dist_Field

    delete cropped;

    // Creating a 2x4 Cropped DF from original Dist_Field
    debug_txt << "Actual Input Parameters for Cropped DF: "<< vcl_endl;
    debug_txt << "Input: Previously Created 4x4 DF" << vcl_endl;
    debug_txt << "Starting Location For Crop (Top Left Corner) In x,y Coordinates: 1,0" <<vcl_endl;
    debug_txt << "Size of Crop:  2x4" << vcl_endl << vcl_endl;

    debug_txt << "Expecting Output DF to Look Like: " << vcl_endl;
    debug_txt << "Channel 0: " << vcl_endl;
    debug_txt << "|1|0|" << vcl_endl << "|1|0|" << vcl_endl << "|1|0|" << vcl_endl << "|1|0|" << vcl_endl;
    debug_txt << "Channel 1: " << vcl_endl;
    debug_txt << "|0|1|" << vcl_endl << "|0|1|" << vcl_endl << "|0|1|" << vcl_endl << "|0|1|" << vcl_endl << vcl_endl;

    // Creating new Dist_Field object to be the cropped version
    cropped = new Dist_Field(true);
    cropped->DF_Crop(test, 1, 2, 0, 4); // Passing in arguments to create a 2x4 cropped Dist_Field

    delete cropped;

}

// This test driver tests the overload minus operator.
//
// The overloaded minus operator allows a Dist_Field object to be subtracted
// from another Dist_Field object.  The returned value represents the "distance"
// between the two Dist_Field objects.  This value is returned as a float.
void testSubtract(){

    debug_txt << "Simple Cases to Test Subtract Operator Overload" << vcl_endl;
    debug_txt << "Setting up 2x2x1 vil_image_view with following pixel values: " << vcl_endl;
    debug_txt << "|0|255|" << vcl_endl << "|0|255|" << vcl_endl << vcl_endl;

    // Create dummy image
    vil_image_view<unsigned char> image(2,2,1);
    image.fill(0);
    image(1,0) = 255;
    image(1,1) = 255;

    debug_txt << "Creating Identical Dist_Field's From Dummy Image With Following Parameters:" << vcl_endl;
    debug_txt << "Size:  2x2 " << vcl_endl;
    debug_txt << "Channels: 2" << vcl_endl << "Channel Width: 128" << vcl_endl;
    debug_txt << "Spatial Blur Width:  0       Spatial Blur Sigma:  1" << vcl_endl;
    debug_txt << "Colour Blur Width :  0       Colour Blur Sigma :  0.625" << vcl_endl << vcl_endl;


    Dist_Field left(true);
    left.Init(image, 2, 0, 0);
    Dist_Field leftcopy(true);
    leftcopy.Init(image,2, 0, 0);

    float distance;
    debug_txt << "Performing Subtraction: " << vcl_endl;
    distance = left - leftcopy;
    debug_txt << "Expected Difference: 0" << vcl_endl;
    debug_txt << "Actual Difference: " << distance << vcl_endl << vcl_endl;

    debug_txt << "Creating A Mirror vil_image_view Of First Case With Values: " << vcl_endl;
    debug_txt << "|255|0|" << vcl_endl << "|255|0|" << vcl_endl << vcl_endl;

    image.fill(0);
    image(0,0) = 255;
    image(0,1) = 255;

    debug_txt << "Creating Dist_Field From Mirror Image With Following Parameters:" << vcl_endl;
    debug_txt << "Size:  2x2 " << vcl_endl;
    debug_txt << "Channels: 2" << vcl_endl << "Channel Width: 128" << vcl_endl;
    debug_txt << "Spatial Blur Width:  0       Spatial Blur Sigma:  1" << vcl_endl;
    debug_txt << "Colour Blur Width :  0       Colour Blur Sigma :  0.625" << vcl_endl << vcl_endl;


    Dist_Field mirror(true);
    mirror.Init(image,2,0,0);

    debug_txt << "Performing Subtraction: " << vcl_endl;
    distance = left - mirror;
    debug_txt << "Expected Difference: 8" << vcl_endl;
    debug_txt << "Actual Difference: " << distance << vcl_endl << vcl_endl;

    debug_txt << "Creating A Mirror vil_image_view Of First Case With Values: " << vcl_endl;
    debug_txt << "|255|0|" << vcl_endl << "|255|0|" << vcl_endl << vcl_endl;

    debug_txt << "Creating Dist_Field From Image With Top Row Identical, Bottom Row Mirrored" << vcl_endl;
    debug_txt << "Size:  2x2 " << vcl_endl;
    debug_txt << "Channels: 2" << vcl_endl << "Channel Width: 128" << vcl_endl;
    debug_txt << "Spatial Blur Width:  0       Spatial Blur Sigma:  1" << vcl_endl;
    debug_txt << "Colour Blur Width :  0       Colour Blur Sigma :  0.625" << vcl_endl << vcl_endl;


    image.fill(0);
    image(1,0) = 255;
    image(0,1) = 255;

    Dist_Field half(true);
    half.Init(image,2,0,0);

    debug_txt << "Performing Subtraction: " << vcl_endl;
    distance = left - half;
    debug_txt << "Expected Difference: 4" << vcl_endl;
    debug_txt << "Actual Difference: " << distance << vcl_endl << vcl_endl;
}
//****************************************************************//
//*************** END Test Drivers For Dist_Fields ***************//
//****************************************************************//

/********** END OF FILE **********/
