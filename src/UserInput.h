// UserInput.h
#ifndef USERINPUT_H_INCLUDED
#define USERINPUT_H_INCLUDED

// Include all headers required
#include <vil/vil_load.h>

#include <stdlib.h>
#include <typeinfo>
#include <sstream>
#include <cstdio>
#include <iostream>
#include <fstream>
#include <string>

// Define the namespace to be used
using namespace std;

/***********************************************/
/********** END OF FUNCTION prototypes**********/
/***********************************************/

/* txtGrabInput Function **********************************************************************
	Will retrieve input arguments from a supplied text file. These values are then stored by reference to the function inputs.
	
	Inputs:
	*:- search_dist: Distance to search for movement
	*:- learning_rate: Rate at which the algorithm will learn
	*:- channels: Number of Distribution channels in the Distribution Field
	*:- spatial_blur: Amount of spatial blur in Distribution Field
	*:- colour_blur: Amount of colour blur in Distribution Field
	*:- input_path: Input path, i.e. C:/somefiles/
	*:- output_path: Output path, i.e. C:/output/
	*:- glob: Input glob, i.e. *png, this will get all png's.
	*:- start_x: The top left hand X location of object to be tracked
	*:- start_y: The top left hand Y location of object to be tracked
	*:- object_width: The width of the object being tracked
	*:- object_height: The height of the object being tracked
	*:- output_yn: Do you want tracker to be saved i.e. y, yes.
	*:- trackColour: Do you want to track colour images or greyscale
	*:- EDFT_yn: Use EDFT function to track object

	Outputs: Nothing, function is void.
*/
void txtGrabInput(unsigned int& search_dist, float& learning_rate, unsigned int& channels, unsigned int& spatial_blur, unsigned int& colour_blur, \
	vcl_string& input_Path, vcl_string& output_Path, vcl_string& glob, unsigned int& start_x, unsigned int& start_y, unsigned int& object_width, \
	unsigned int& object_height, vcl_string& output_yn, vcl_string& trackColour, vcl_string& EDFT_yn);

/* txtGrabInput Function **********************************************************************
	Will retrieve input arguments from a supplied text file. These values are then stored by reference to the function inputs.
	Inputs:
	*:- input_path: Input path, i.e. C:/somefiles/
	*:- start_x: The top left hand X location of object to be tracked
	*:- start_y: The top left hand Y location of object to be tracked
	*:- object_width: The width of the object being tracked
	*:- object_height: The height of the object being tracked

	Outputs: Nothing, function is void.
*/
void txtGrabInputCoords(vcl_string in_path, unsigned int& start_x, unsigned int& start_y, unsigned int& object_width, unsigned int& object_height);
#endif // USERINPUT_H_INCLUDED  
/***********************************************/
/********** END OF FUNCTION prototypes**********/
/***********************************************/
