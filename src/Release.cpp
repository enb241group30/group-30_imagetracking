// Please put all includes needed for main to run below:
#include "DF_Model.h"
#include "DF_Tracker.h"
#include "Dist_Field.h"
#include "UserInput.h"

#include <vil/vil_image_view.h>
#include <vil/vil_load.h>
#include <vil/vil_save.h>

#include <vul/vul_file_iterator.h>
#include <vul/vul_file.h>

#include <vcl_vector.h>

#include <vul/vul_arg.h>

#include <stdlib.h>
#include <typeinfo>
#include <sstream>
#include <cstdio>
#include <iostream>
#include <fstream>
#include <string>

using namespace std;

/*
 * Main program to run the Group #30's Single Object Tracking Algorithm (SOTA).
 * This program will
 *  - take a list of images on the command line, specified as a path and a partial filename (i.e. *.png to get all png files)
 *  - take a set of parameters for the Distribution Field (DF) tracking from the command line, if no value is given defaults will be used
 *  - will save a set of images showing the motion segmented output
 */

/***********************************/
/********** START OF MAIN **********/
/***********************************/

int main (int argc, char * argv[])
{
	// Create the varible for use in main
		float learning_rate;
		vcl_string input_Path;
		vcl_string output_Path;
		vcl_string output_yn;
		vcl_string glob;
		vcl_string trackColour;
		vcl_string EDFT_yn;
		unsigned int search_dist;
		unsigned int colour_blur;
		unsigned int spatial_blur;
		unsigned int channels;
		unsigned int start_x;
		unsigned int start_y;
		unsigned int object_width;
		unsigned int object_height;

	// Ask user if they provided a config file
	vcl_cout << " Do you have a configuration file? (y/n)" << vcl_endl;
	char yn;	// yes or no input
	vcl_cin >> yn;
	// check that input is y, Y, n or N
	while(!(yn == 'y' || yn == 'Y' || yn == 'n' || yn == 'N'))
	{
		vcl_cout << "Input invalid please try again." << vcl_endl;
		vcl_cin >> yn;
	}
	// If user said yes proceed to use config file
	if (yn == 'y' || yn == 'Y')
	{
		txtGrabInput(search_dist, learning_rate, channels, spatial_blur, colour_blur, input_Path, output_Path, \
			glob, start_x, start_y, object_width, object_height, output_yn, trackColour, EDFT_yn);
	// If user said no, leave function.
	} else if (yn == 'n' || yn == 'N')
	{
		vcl_cout << "You have chosen not to supply a configuration file, input parameters will now be taken from command line arguments" << vcl_endl;

		// specify all the arguments you expect/need
		vul_arg<vcl_string>
			arg_in_path("-path", "Input path, i.e. C:/somefiles/"),
			arg_out_path("-outpath", "Output path, i.e. C:/output/"),
			arg_output_yn("-output_yn", "Do you want tracker to be saved i.e. y, yes.", "n"),
			arg_in_glob("-glob", "Input glob, i.e. *png, this will get all png's."),
			arg_trackColour("-tc", "Do you wish to track colour image? (y/n)", "n"),
			arg_EDFT("-edft","Do you want to use EDF tracking? (Y/N)", "n");

		// Set up of all input arguments and their defaults
		vul_arg<unsigned> arg_search_dist("-sd", "Distance to search for movement", 30),
						  arg_channels("-c", "Number of Distribution channels in the Distribution Field", 8),
						  arg_spatial_blur("-sb", "Amount of spatial blur in Distribution Field", 4),
						  arg_colour_blur("-cb", "Amount of colour blur in Distribution Field", 1),
						  arg_start_x("-sx", "The top left hand X location of object to be tracked"),
						  arg_start_y("-sy", "The top left hand Y location of object to be tracked"),
						  arg_object_width("-ow", "The width of the object being tracked"),
						  arg_object_height("-oh", " The height of the object being tracked");

		vul_arg<float> 	  arg_learning_rate("-lr", "Rate at which the algorithm will learn", 0.05);

		/* call vul_arg_parse(argc, argv) to parse the arguments, this will go through the command line string, look for all the specified argument string,
		*  and extract the provided values. */
		vul_arg_parse(argc, argv);

		/* now we can start to access our arguments, to do it, simply call the created vul_arg object. i.e. arg_in_path() will return the value that was found after the path argument.
		* So, the command line: "<program name> -path c:\nothing -glob * -n 6 -f 8" would yield the following:
		*		arg_in_path() 		-> c:\nothing
		*		arg_in_glob() 		-> *
		*		arg_number()  		-> 6
		*		arg_number_again()	-> 4 			(NOTE: default value is used as this paramter is not specified on the command line)
		*		arg_float()			-> 8.0
		*
		* we can use this to check that required variables were provided
		* if arg_in_path() or arg_in_glob() are empty strings, it means they weren't provided */
		if (((arg_in_path() == "") || (arg_in_glob() == "")) || (arg_out_path() == "") || (arg_start_x() == NULL) \
			|| (arg_start_y() == NULL) || (arg_object_width() == NULL) || (arg_object_height() == NULL) )
		{
			/* if we need these arguments to proceed, we can now exit and print the help as we quit. vul_arg_display_usage_and_exit() will display the available arguments
			*  alongside the help message specified when the vul_arg objects were created*/
			vcl_cerr << "\nMissing command line argument\n" << vcl_endl;
			vul_arg_display_usage_and_exit();
		}

		/* Set all default values from input args. These are set using command
		* line arguments. If no input is supplied for any argument a default
		* value will be used.
		*/
		learning_rate = arg_learning_rate();
		input_Path = arg_in_path();
		output_Path = arg_out_path();
		output_yn = arg_output_yn();
		glob = arg_in_glob();
		trackColour = arg_trackColour();
		EDFT_yn = arg_EDFT();
		search_dist = arg_search_dist();
		colour_blur = arg_colour_blur();
		spatial_blur = arg_spatial_blur();
		channels = arg_channels();
		start_x = arg_start_x();
		start_y = arg_start_y();
		object_width = arg_object_width();
		object_height = arg_object_height();

	}
		// Print stored values to screen for user to see.
		vcl_cout << "\nThe following will be used as inputs to the program:-\n";
		vcl_cout << "Search Distance = " << search_dist << vcl_endl;
		vcl_cout << "Learning Rate = " << learning_rate << vcl_endl;
		vcl_cout << "Colour Blur = " << colour_blur << vcl_endl;
		vcl_cout << "Spatial Blur = " << spatial_blur << vcl_endl;
		vcl_cout << "No. Channels = " << channels << vcl_endl;
		vcl_cout << "Input Path = " << input_Path << vcl_endl;
		vcl_cout << "Output Path = " << output_Path << vcl_endl;
		vcl_cout << "Output Saving = " << output_yn << vcl_endl;
		vcl_cout << "Input file extension = " << glob << vcl_endl;
		vcl_cout << "Object Start x = " << start_x << vcl_endl;
		vcl_cout << "Object Start_y = " << start_y << vcl_endl;
		vcl_cout << "Object Width = " << object_width << vcl_endl;
		vcl_cout << "Object Height = " << object_height << vcl_endl;



		// Parsing a directory of images \/ \/ \/ \/ \/
		// this is a list to store our filenames in
		vcl_vector<vcl_string> filenames;

		// Get the directory we're going to search, and what we're going to search for. We'll take these from the vul_arg's we used above
		vcl_string directory = input_Path;
		vcl_string extension = glob;

		// loop through a directory using a vul_file_iterator, this will create a list of all files that match "directory + "/*" + extension", i.e. all files in the directory
		// that have our target extension
		for (vul_file_iterator fn=(directory + "/*" + extension); fn; ++fn)
		{
			// we can check to make sure that what we are looking at is a file and not a directory
			if (!vul_file::is_directory(fn()))
			{
				// if it is a file, add it to our list of files
				filenames.push_back (fn());

			}
		}

		/* filenames now contain all of the files with our target extension in our directory,
		* if we want to loop through them, we can now do */

	    //  DF_Tracker::DF_Tracker(unsigned int channels, unsigned int spatial_blur,
	    //   unsigned int colour_blur, unsigned int search_dist, float learning_rate
        // ****************** Colour = bool true or false
	    DF_Tracker Tracker(channels, spatial_blur, colour_blur, search_dist, learning_rate);

	    // Initiates our Tracker
		//Init(vil_image_view<unsigned char> image, int start_x, int start_y, int width, int height);
		vcl_cout << "Processing image: " << filenames[0].c_str() << vcl_endl;
		vil_image_view<unsigned char> FirstFrame = vil_load(filenames[0].c_str());
	    Tracker.Init(FirstFrame, start_x, start_y, object_width, object_height);
		//Cout input variables for user
		vcl_cout << "Input path:" << directory << vcl_endl;
		vcl_cout << "File extension specified:" << extension << vcl_endl;


		for (int i = 1; i < filenames.size(); i++)
		{
			// Load in the first image found at filename[i]
			vcl_cout << "Processing image: " << filenames[i].c_str() << vcl_endl;
			vil_image_view<unsigned char> CurrentImage = vil_load(filenames[i].c_str());

			Tracker.Track(CurrentImage,i,output_Path,output_yn);
		}

/*********************************/
/********** END OF MAIN **********/
/*********************************/
}
