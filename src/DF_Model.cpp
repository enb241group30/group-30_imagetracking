// DF_Model.cpp

#include "DF_Model.h"

#include <vil/vil_image_view.h>

#include <vcl_fstream.h>




DF_Model::DF_Model(unsigned int channels, unsigned int spatial_blur, unsigned int colour_blur, unsigned int search_dist, float learning_rate)
                : _channels(channels), _spatial_blur(spatial_blur), _colour_blur(colour_blur), _search_dist(search_dist), _learning_rate(learning_rate)
{
    // initialise all member variables

}

DF_Model::~DF_Model()
{
    // delete _DF
    delete _DF;
}

void DF_Model::Init(vil_image_view<unsigned char> image, unsigned int start_x, unsigned int start_y)
{
    // get width and height of image
    _width = image.ni();
    vcl_cout << "DF_Model's width is " << _width << vcl_endl;
    _height = image.nj();
    // initialise _xloc and _yloc
    _xloc = start_x;
    _yloc = start_y;
    // allocate and initialise _DF member variable
    _DF = new Dist_Field;
    _DF->Init(image, _channels, _spatial_blur, _colour_blur);
}

void DF_Model::updateLoc(unsigned int x, unsigned int y)
{
    //update member variables with input args
    _yloc = y;
    _xloc = x;
}

int DF_Model::x() const
{
    //return x location
    return _xloc;
}

int DF_Model::y() const
{
    //return y location
    return _yloc;
}

Dist_Field DF_Model::DF() const
{
    //return DF
    return _DF[0];
}

void DF_Model::DF_update(Dist_Field newDF)
{
    //update _DF with input arg
    _DF->DF_Update(newDF, _learning_rate);
}




