// UserInput.cpp
#include "UserInput.h"

/******************************************************/
/********** START OF FUNCTION Implementation **********/
/******************************************************/

/* txtGrabInput Function **********************************************************************
	Will retrieve input arguments from a supplied text file. These values are then stored by reference to the function inputs.
	
	Inputs:
	*:- search_dist: Distance to search for movement
	*:- learning_rate: Rate at which the algorithm will learn
	*:- channels: Number of Distribution channels in the Distribution Field
	*:- spatial_blur: Amount of spatial blur in Distribution Field
	*:- colour_blur: Amount of colour blur in Distribution Field
	*:- input_path: Input path, i.e. C:/somefiles/
	*:- output_path: Output path, i.e. C:/output/
	*:- glob: Input glob, i.e. *png, this will get all png's.
	*:- start_x: The top left hand X location of object to be tracked
	*:- start_y: The top left hand Y location of object to be tracked
	*:- object_width: The width of the object being tracked
	*:- object_height: The height of the object being tracked
	*:- output_yn: Do you want tracker to be saved i.e. y, yes.
	*:- trackColour: Do you want to track colour images or greyscale
	*:- EDFT_yn: Use EDFT function to track object

	Outputs: Nothing, function is void.
*/
void txtGrabInput(unsigned int& search_dist, float& learning_rate, unsigned int& channels, unsigned int& spatial_blur, unsigned int& colour_blur, \
	vcl_string& input_Path, vcl_string& output_Path, vcl_string& glob, unsigned int& start_x, unsigned int& start_y, unsigned int& object_width, \
	unsigned int& object_height, vcl_string& output_yn, vcl_string& trackColour, vcl_string& EDFT_yn){
	// Variable decelerations
	vcl_string fileLoc = "";	// File location string
	vcl_string line = "";		// String container for use with getline()
	vcl_string specifier = "";	// String to store specifier from line
	vcl_string stemp = "";		// Used to store temp strings
	ifstream file;				// Input file stream object
	stringstream input;			// String stream object
	float temp = 0;		// Used to store float input from file
	//char yn;			// yes or no input
	int counter = 0;	// used to count number of getline() loops made
	int expected = 11;	// expected number of getline() loops i.e. number of input variables

			// Get .txt file location from user
	vcl_cout << "Please specify input .cfg path: ";
	vcl_cin >> fileLoc;
	// Open file at path 'fileLoc'
	file.open(fileLoc.c_str());
	// Error check to ensure path is okay and if not valid prompt user for valid path
	while (!file)
	{
		vcl_cerr << "File path not valid please enter valid path: ";
		getline(vcl_cin,fileLoc);
		file.open(fileLoc.c_str());
	}
	// loop through open file printing each line of file
	if (file.is_open()){
		while (getline(file,line,'\n')){
			input << line;		// Move string from getline into string stream
			input >> specifier;	// Pull specifier out of stream
			input >> stemp;		// Pull value out of stream
			temp = atof(stemp.c_str());
			input.str("");	// Erase the string stream buffer
			input.clear();	// Clear error flags if any
			counter++;

			// Sort the values from the line using specifier
			if (specifier == "COLOUR_BLUR")
			{
				colour_blur = temp;
			} else if (specifier == "SPATIAL_BLUR")
			{
				spatial_blur = temp;
			} else if (specifier == "CHANNELS")
			{
				channels = temp;
			} else if (specifier == "INPUT_PATH")
			{
				input_Path = stemp;
			} else if (specifier == "OUTPUT_PATH")
			{
				output_Path = stemp;
			} else if (specifier == "GLOB")
			{
				glob = stemp;
			} else if (specifier == "SEARCH_DIST")
			{
				search_dist = temp;
			} else if (specifier == "LEARNING_RATE")
			{
				learning_rate = temp;
			} else if (specifier == "OUTPUT")
			{
				output_yn = stemp;
			} else if (specifier == "TRACK_COLOUR")
			{
				trackColour = stemp;
			} else if (specifier == "EDFT")
			{
				EDFT_yn == stemp;
			} else {
				vcl_cerr << "Invalid specifier please check config file" << vcl_endl;	// Possible use of catch throw statment here??
				 exit (EXIT_FAILURE);
			}
		}
		// Error checking. Check to make sure right number of inputs are there
			if (counter > expected || counter < expected)
			{
				if (counter < expected)
				{
					vcl_cerr << "You have too few inputs in configuration file. \nPlease check file and ensure there are "<< expected << " lines." << vcl_endl;
					 exit (EXIT_FAILURE);
				}else{
				vcl_cerr << "You have too many inputs in configuration file. \nPlease check file and ensure there are "<< expected << " lines." << vcl_endl;
				 exit (EXIT_FAILURE);
				}
			}
		file.close();

		// Retrieve object co-ordinates and size
		txtGrabInputCoords(input_Path, start_x, start_y, object_width, object_height);

	} else{
		vcl_cout << "Unable to open file";
		exit (EXIT_FAILURE);
	} 
}


/* txtGrabInput Function **********************************************************************
	Will retrieve input arguments from a supplied text file. These values are then stored by reference to the function inputs.
	Inputs:
	*:- input_path: Input path, i.e. C:/somefiles/
	*:- start_x: The top left hand X location of object to be tracked
	*:- start_y: The top left hand Y location of object to be tracked
	*:- object_width: The width of the object being tracked
	*:- object_height: The height of the object being tracked

	Outputs: Nothing, function is void.
*/
void txtGrabInputCoords(vcl_string in_path, unsigned int& start_x, unsigned int& start_y, unsigned int& object_width, unsigned int& object_height){
	// Varible declerations
	vcl_string fileLoc;
//vcl_cout << in_path << vcl_endl;
	vcl_string line = "";
	ifstream file;
	stringstream input;
	stringstream ss;
	char buff = ',';
	double value1 = 0;
	double value2 = 0;
	double value3 = 0;
	double value4 = 0;

	// Append the groundtruth.txt file name to the image folder path stored in fileLoc
	ss << in_path << "\\groundtruth.txt";
	ss >> fileLoc;
//vcl_cout << fileLoc << vcl_endl;
	// Open file @ fileLoc
	file.open(fileLoc.c_str());
	// Error check, if failed to open file leave function.
	if (!file)
	{
		vcl_cerr << "Ground truth file path not valid please check configuration file.";
		exit (EXIT_FAILURE);
			// Possible place to use catch throw statement???
	}

	if (file.is_open())
	{
		getline(file, line, '\n');
		input << line;	// Push string into string stream
		input >> value1 >> buff >> value2 	//Pull values out of stream discarding the char characters
			>> buff >> value3 >> buff >> value4;
		// Store the values pulled from stream into external varibles
		start_x = round(value1);
		start_y = round(value2);
		object_width = round(value3);
		object_height = round(value4);
		// Clean up string stream
		input.str("");	// Erase the string stream buffer
		input.clear();	// Clear error flags if any
		// Close file
		file.close();
	}else{
			vcl_cout << "Unable to open file";
			exit (EXIT_FAILURE);
		}

	file.open(fileLoc.c_str());

}
/***************************************************/
/********** END OF FUNCTION Implementation**********/
/***************************************************/
