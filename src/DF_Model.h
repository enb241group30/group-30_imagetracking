#ifndef DF_MODEL_H
#define DF_MODEL_H

#include "Dist_Field.h"

#include <vil/vil_image_view.h>

class DF_Model{
public:

    /* Constructor************************************************************
    This is the default constructor for the DF_Model class, it takes parameters
    which are used during other member function and saves them as member variables.
    Inputs:
    *:- channels = used to initialise member variable _channels
    *:- spatial_blur = used to initialise member variable _spatial_blur
    *:- colour_blur = used to initialise member variable _colour_blur
    *:- search_dist = used to initialise member variable _search_dist
    *:- learning_rate = used to initialise member variable _learning_rate */
	DF_Model(unsigned int channels, unsigned int spatial_blur, unsigned int colour_blur, unsigned int search_dist, float learning_rate);

	/* Destructor**************************************************************
	This is the destructor for the DF_Model class, it simply frees any dynamically
	allocated memory to prevent memory leaks.
	NOTE: all dynamic memory must be deleted to prevent memory leaks/seg faults
	Dynamic memory to delete:
	*1) _DF
	*/
	~DF_Model();

    /* Init function***********************************************************
    Init takes an image, along with a set of coordinates. It then creates a model
    of the current image, based on the size and coordinates of the input image.
    The model is initialised as an section of the current frame, based on the size
    of the input image and the x and y coordinates.
    Inputs:
    *:- image = image(normally cropped one) which is a crop of the current frame
    *:- start_x = the starting x coordinate of the image
    *:- start_y = the starting y coordinate of the image */
	void Init(vil_image_view<unsigned char> image, unsigned int start_x, unsigned int start_y);
	void ColourInit(vil_image_view<unsigned char> image, unsigned int start_x, unsigned int start_y);


    /* DF_Update function*******************************************************
    DF_Update takes a Dist_Field object and updates the member variable Dist_Field
    DF with the object which it is passed
    Inputs:
    *:- Dist_Field = Dist_Field object which updates current model */
	void DF_update(Dist_Field);

    /* x function ***************************************************************
    x takes no input arguments, but it returns the current x location. This is
    necessary due to the _xloc member variable being protected.
    Outputs:
    *:- int = _xloc */
	int x() const;

	/* y function ***************************************************************
    y takes no input arguments, but it returns the current y location. This is
    necessary due to the _yloc member variable being protected.
    Outputs:
    *:- int = _yloc */
	int y() const;

    /* updateLoc function********************************************************
    the updateLoc function takes two ints and updates the member variables _xloc
    and _yloc using the input arguments.
    Inputs:
    *:- x = variable to update _xloc with
    *:- y = variable to update _yloc with */
	void updateLoc(unsigned int x, unsigned int y);

	// Public Members
	Dist_Field DF() const;

protected:

    //Protected Members
	unsigned int _width;
	unsigned int _height;
	unsigned int _xloc;
	unsigned int _yloc;

	const unsigned int _channels;
	const unsigned int _spatial_blur;
	const unsigned int _colour_blur;
	const unsigned int _search_dist;
	const float _learning_rate;

	Dist_Field* _DF;




};

#endif
