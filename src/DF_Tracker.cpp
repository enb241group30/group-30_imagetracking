#include "DF_Tracker.h"
#include "Dist_Field.h"

#include <vil/vil_crop.h>
#include <vil/vil_image_view.h>
#include <vil/vil_load.h>
#include <vil/vil_save.h>

#include <vul/vul_file_iterator.h>
#include <vul/vul_file.h>

#include <stdlib.h>
#include <typeinfo>
#include <sstream>
#include <cstdio>
#include <iostream>
#include <fstream>
#include <string>
#include <cstring>

DF_Tracker::DF_Tracker(unsigned int channels, unsigned int spatial_blur, unsigned int colour_blur, unsigned int search_dist, float learning_rate)
                : _channels(channels), _spatial_blur(spatial_blur), _colour_blur(colour_blur), _search_dist(search_dist), _learning_rate(learning_rate)
{
	//Initialise all member variables

}

DF_Tracker::~DF_Tracker()
{
	//delete model
	delete model;
}

void DF_Tracker::Init(vil_image_view<unsigned char> image, unsigned int start_x, unsigned int start_y, unsigned int width, unsigned int height)
{
	//save x and y locations
	_prev_x = start_x;
	_prev_y = start_y;

	//save width and height
	_width = width;
	_height = height;

	vcl_cout << "Initialising DF Tracker With Following Paramters: " << vcl_endl;
	vcl_cout << "Starting Coordinates (x,y): " << start_x << "," << start_y << vcl_endl;
	vcl_cout << "Size width,height: " << width << "," << height << vcl_endl;

	//create cropped version of input image.
	vil_image_view<unsigned char> Cropped_Img = vil_crop(image, start_x, width, start_y, height);
	//dynamically allocate memory for model using DF_Model constructor
	model = new DF_Model(_channels, _spatial_blur, _colour_blur, _search_dist, _learning_rate);
	//initialise model
	model->Init(Cropped_Img, start_x, start_y);
}

void DF_Tracker::Track(vil_image_view<unsigned char> image, int& counter, vcl_string output_path, vcl_string yn)
{
	//crop image to size (2*max_search_dist + width) x (2*max_search_dist + height)
	//create DF of cropped image NOTE: cropped image is bigger than model
	//DF = DF of cropped vil_img_view, cropped_DF = image size cropped DF
	//dist_field(DF,cropped_DF)

	/* Run through gradient descent search in the track function*/

	// need to loop through all x locations, then all y locations
	//1) evaluate distance at each location, starting from its previous location
	//2) location with lowest distance is new location of the object being tracked
	//3) Update new x or y location which corresponds to the lowest distance

	int count = _search_dist;

	//initialise best distance
	double Best_Dist = 99999999; //make it huge so overwritten straight away

	// Create DF of current frame
	Dist_Field DF;
	DF.Init(image, _channels, _spatial_blur, _colour_blur);

	//Create location variables which we can alter
	int x_rgt_loc, x_lft_loc, y_pos_loc, y_neg_loc;
	int current_pos_x = _prev_x;
	int current_pos_y = _prev_y;
	double distup, distdwn, distrgt, distlft, distcrt;

//vcl_cout << "Starting While Loop" << vcl_endl;

	//loop while distance is decreasing
	do
	{
		//Create pixel locations
		x_lft_loc = current_pos_x - 1;
		x_rgt_loc = current_pos_x + 1;
		y_pos_loc = current_pos_y + 1;
		y_neg_loc = current_pos_y - 1;

		//Check all pixel locations are valid.
		if(x_lft_loc < 0)
        {
            x_lft_loc = 0;
        }
        if(x_rgt_loc == _width)
        {
            x_rgt_loc = _width - 1;
        }
        if(y_pos_loc == _height)
        {
            y_pos_loc = _height - 1;
        }
        if(y_neg_loc < 0)
        {
            y_neg_loc = 0;
        }

		//create cropped image on all 4 sides of current pixel and current pixel
		Dist_Field curr_DF;
		Dist_Field rgt_DF;
		Dist_Field lft_DF;
		Dist_Field up_DF;
		Dist_Field dwn_DF;

		curr_DF.DF_Crop(DF, current_pos_x, _width, current_pos_y, _height); // current pixel
		rgt_DF.DF_Crop(DF, x_rgt_loc, _width, current_pos_y, _height); //right pixel
		lft_DF.DF_Crop(DF, x_lft_loc, _width, current_pos_y, _height); //left pixel
		up_DF.DF_Crop(DF, current_pos_x, _width, y_pos_loc, _height); //upper pixel
        dwn_DF.DF_Crop(DF, current_pos_x, _width, y_neg_loc, _height); //lower pixel

		//calculate distances

		distcrt = model->DF() - curr_DF;
		distup  = model->DF() - up_DF;
		distdwn = model->DF() - dwn_DF;
		distrgt = model->DF() - rgt_DF;
		distlft = model->DF() - lft_DF;

		//get the smallest distance
		if(distcrt < distup && distcrt < distdwn && distcrt < distrgt && distcrt < distlft && distcrt < Best_Dist)
			{
				//set new best distance
				Best_Dist = distcrt; // current distance is best
				//x and y do not need updating as they are still current_pos_x
			}
		if(distup < distcrt && distup < distdwn && distup < distrgt && distup < distlft && distup < Best_Dist)
			{
				//set new best distance
				Best_Dist = distup; //upper distance is best
				//update y location as best place of object
				current_pos_y = y_pos_loc;
			}
		if(distdwn < distcrt && distdwn < distup && distdwn < distrgt && distdwn < distlft && distdwn < Best_Dist)
			{
				//set new best distance
				Best_Dist = distdwn; //lower distance is best
				//update y location as best place of object
				current_pos_y = y_neg_loc;
			}
		if(distrgt < distcrt && distrgt < distup && distrgt < distdwn && distrgt < distlft && distrgt < Best_Dist)
			{
				//set new best distance
				Best_Dist = distrgt; //right distance is best
				//update x location as best place of object
				current_pos_x = x_rgt_loc;
			}
		if(distlft < distcrt && distlft < distup && distlft < distdwn && distlft < distrgt && distlft < Best_Dist)
			{
				//set new distance
				Best_Dist = distlft; //left distance is best
				//update x location as best place of object
				current_pos_x = x_lft_loc;
			}
			//decrement our count
			count--;
	}while(count > 0 && Best_Dist != distcrt);

	//once location for image has been found, update previous location of x and y to this location
//vcl_cout << "New Location: " << current_pos_x << "," << current_pos_y << vcl_endl;

	_prev_x = current_pos_x;
	_prev_y = current_pos_y;

	/*update the DF model to keep tracking consistent*/
	// Create cropped dist_field at new location
	Dist_Field New_DF;
	New_DF.DF_Crop(DF, current_pos_x, _width, current_pos_y, _height);

	//Update model with new dist_field
	model->DF_update(New_DF);

	//update x and y location for model
	model->updateLoc(current_pos_x, current_pos_y);

	//Print the image with bounding box round image
	DrawBox(image, output_path, counter,yn);

}

void DF_Tracker::DrawBox(vil_image_view<unsigned char> image, vcl_string output_path, int& count, vcl_string yn) const{
	// Store function varibles
	unsigned int width = _width;
	unsigned int height = _height;
	unsigned int x = _prev_x;
	unsigned int y = _prev_y;
	unsigned int imageWidth = image.ni();
	unsigned int imageHeight = image.nj();
	unsigned int NoPlanes = image.nplanes();
	char fname[20];

	// Check and ensure user wants the output saved.
	if(yn == "y" || yn == "Y" || yn == "yes" || yn == "Yes" || yn == "ON" || yn == "On" || yn == "on")
	{
		// Change pixel values for the box region
		for (int i = 0; i < imageWidth; ++i)
		{
			//error check for x boundaries
			if (i > imageWidth)
			{
				vcl_cout<<"Out of X bounds";
				exit (EXIT_FAILURE);
			}
			for (int j = 0; j < imageHeight; ++j)
			{
				//error check for y boundaries
				if (j > imageHeight)
				{
					vcl_cout<<"Out of Y bounds";
					exit (EXIT_FAILURE);
				}
				// If the loop falls along a border change the value at the location
				if ( (x <= i) && (y <= j) && (i <= (x + width)) && (j <= (y + height)) )
				{
					if((x == i) || (i == (x + width)) || (y == j) || (j == (y + height)))
					{
						for (int n = 1; n < NoPlanes; ++n)
						{
							image(i,j,n) = 0;	// Set the Green and Blue channels to 0. Will make transparent red box.
						}
					}
				}
			}
		}
	// If user choses not to save output make sure this choice is printed to console.
	}else if(yn == "n" || yn == "N" || yn == "no" || yn == "No" || yn == "NO" || yn == "OFF" || yn == "Off" || yn == "off")
	{
		vcl_cout<<"User has chosen NOT to output image with mark-up showing tracking output" <<vcl_endl;
		return;
	// If none of the above conditions are met error is present. Prompt user to check config file
	}else{ vcl_cerr << "Input: "<<yn<<" not valid please check config file."<<vcl_endl; return;}

	std::sprintf(fname,"\\a00000%1d.bmp",count);
	output_path += fname;
	char tab2[256];
	strncpy(tab2, output_path.c_str(), sizeof(tab2));
	tab2[sizeof(tab2) - 1] = 0;
//vcl_cout << tab2 << vcl_endl;
	vil_save(image, tab2);
}
