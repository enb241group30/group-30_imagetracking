#ifndef DIST_FIELD_H
#define DIST_FIELD_H

#include <vil/vil_image_view.h>
#include <vbl/vbl_array_2d.h>

#include <vcl_vector.h>
#include <vcl_fstream.h>
#include <vnl/vnl_random.h>

// Class to store a distribution Field in
class Dist_Field
{
public:
    /* Constructor ************************************************************************
    This is the default constructor for this class, it simply creates an object and initialises
    _sigma_2d and _sigma_1d to default values of:
    _sigma_2d = 1;
    _sigma_1d = 0.625; */
    Dist_Field();

    /* Constructor ************************************************************************
    Same as default construct but if passed a true operator, will start outputting the values
    stored in the _DF Array to an output text file for troubleshooting/debugging */
    Dist_Field(bool debug); // Construct to enable the debugging output of this function

    /* Constructor ************************************************************************
    This constructor allows the Dist_Field object to be supplised with custom values for the
    2D and 1D sigma blurring.
    Inputs:
	*:- sd_2d = The standard deviation to be applied to the 2D gaussian blurring
	*:- sd_1d = The standard deviation to be applied to the 1D gaussian blurring */
    Dist_Field(double sd_2d, double sd_1d);

    /* Constructor ************************************************************************
    This constructor allows the Dist_Field object to be supplised with custom values for the
    2D and 1D sigma blurring as well as turning on the debug output
    Inputs:
	*:- sd_2d = The standard deviation to be applied to the 2D gaussian blurring
	*:- sd_1d = The standard deviation to be applied to the 1D gaussian blurring */
    Dist_Field(bool debug, double sd_2d, double sd_1d);  // Same as previous constructor but with debugging output

    /* Destructor *************************************************************************
	This is the default destructor for this class, it deletes any dynamically created memory
	NOTE: All dynamic memory must be deleted to prevent memory leaks / seg faults.
	dynamic memory to delete:
	*1) debug_txt: single object to delete. */
    ~Dist_Field();

    /* Init Function **********************************************************************
	Init takes an image, and builds a distribution field(DF) of that object based on the supplied parameters.

	Inputs:
	*:- image = The image to create a DF from
	*:- channels = Number of channels for the DF
	*:- spatial_blur = The width of the 2D gaussian blur
	*:- colour_blur = The width of the 1D guassian blur

	Outputs: Nothing, function is void. */
    void Init(vil_image_view<unsigned char> &image,unsigned int channels, unsigned int spatial_blur, unsigned int colour_blur);


    void ColourInit(vil_image_view<unsigned char> &image,unsigned int channels, unsigned int spatial_blur, unsigned int colour_blur);

    /* DF_Crop Function *
    *********************************************************************
	DF_Crop transforms the current Dist_Field object into a cropped version of another Dist_Field.

	DF_Crop requires an input of a Dist_Field, along with coordinates that specify where in the original
	Dist_Field to start cropping from, as well as the size to crop out.

	The new cropped Dist_Field object does not need to be initialised, only constructed as it is merely copying
	values from the original Dist_Field object to create a smaller one.

	Inputs:
	*:- input = The Dist_Field object to create a cropped version of
	*:- x = The x coordinate of where the crop starts (Top left corner)
	*:- width = The width of the crop to be taken
	*:- y = The y coordinate of where the crop starts (Top Left Corner)
	*:- height = The height of the crop to be taken

	Outputs: Nothing, function is designed to operate on the object that calls it */
    void DF_Crop(Dist_Field& input, unsigned int x, unsigned int width, unsigned int y, unsigned int height);

     /* DF_Update Function **********************************************************************
	DF_Update updates this objects DF with information from another Dist_Field.

	Inputs:
	*:- input = The Dist_Field object whose information we want to incorporate into this ones
	*:- learning_rate = A value between to 0-1 which determines how much of the original informaiton
	                    is replaced with information from the new Dist_Field.  0 would be nohing is
	                    replaced, whereas 1 would completely replace the current Dist_Field with
	                    the new information.

	Outputs: Nothing, function is designed to operate on the object that calls it */
    void DF_Update(Dist_Field& input, float learning_rate);


    /* Overloaded operators
    () Operator Overload **********************************************************************
     When called with values (x,y,c) the operator returns the value stored for the pixel location
     x,y in channel c. */
    double operator() (unsigned int x, unsigned int y , int c) const;

    /* - Operator Overload **********************************************************************
     When a Dist_Field object is subtracted from another, the returned value is the "distance" between
     the two objects.  This is merely an implementation of the algorithm that iterates over every pixel
     in every channel of the two objects to calculate a distance as follows:
     Distance += | DF1(x, y, c) � DF2(x, y, c) | */
    double operator - (Dist_Field& df2) const;


protected:
    // Member variables
    double _width;     // Width of the DF
    double _height;    // Height of the DF
    unsigned int _channels;  // Number of channels for the DF
    unsigned int _channel_width; // Width of each channel
    unsigned int _spatial_blur;  // Width of spatial blurring to be applied across a channel
    unsigned int _colour_blur;   // Width of blurring through the channels to be applied

    const double _sigma_2d;           // Standard deviation used for applying 2D blur to a DF channel
    const double _sigma_1d;           // Standard deviation used for applying 1D blur to individual DF pixels

    // Store the actual DF inside as multiple vil_image_views.
    // Each vil_image_view element of the vector represents a single channel
    vcl_vector<vil_image_view<double> > _DF;


    void Blur_2D();  // Helper function which will do a 2D blur of each channel of the DF
    void Blur_1D();  // Help function which will do a 1D blur of each pixel across the channels of the DF

    // An initialised used specifically for setting up a Dist_Field to become a cropped version
    // of another Dist_Field
    void InitCrop(Dist_Field input, unsigned int width, unsigned int height);

    // These functions/members are  used for debugging
    void PrintDFValues() const;  // Print all the values of the _DF array to the txt file
    void SaveDF() const;         // Save the Dist_Field channels as output images

    bool _debug; // Boolean used to enable debugging of the Dist_Field class
    vcl_ofstream* debug_txt; // Create an ofstream object for writing to a .txt file


};


#endif // DIST_FIELD_H
